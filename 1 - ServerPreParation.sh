
### SISTEMI UPDATE EDIB SONRA GITLAB EE - NIN DUZGUN CALIMASI UCUN TELEB OLUNAN PAKETLERI YUKLEYIRIK.

sudo apt update
sudo apt upgrade
sudo apt install ca-certificates curl openssh-server ufw apt-transport-https -y

# UBUNTU SERVERIN SSH PORTUNU 22 - DEN DIGER BIR PORTA DEYISIRIK.
# ILK ONCE SSHD CONFIG FAYLININ EHTIYAT NUSXESINI CIXARIRIQ.

sudo cp  /etc/ssh/sshd_config /etc/ssh/sshd_config.bak

sudo vim /etc/ssh/sshd_config

# SSH PORTU DEYISDIKDEN SONRA UFW YENI DEYISIKLIKLER UCUN AYARLANIR VE SSHD SERVISI RESTART EDILIR.

sudo ufw allow OpenSSH
sudo ufw allow 2222
sudo ufw allow http
sudo ufw allow https
sudo ufw enable

sudo ufw status

sudo systemctl restart sshd


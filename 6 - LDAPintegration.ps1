#### LDAP INTEGRASIYA HAQQINDA: 
https://docs.gitlab.com/ee/administration/auth/ldap/index.html

## SELF SIGNED CERTIFIKATI GENERASIYA EDIRIK.
### KOMANDALARI LDAP SERVEREDE YERINE YETIRIN.
$domain_name = "contoso.com"

$dns_name = $env:computername + '.' + $domain_name;

$mycert=New-SelfSignedCertificate -DnsName $dns_name -CertStoreLocation cert:/LocalMachine/My;

### SERTIFIKATI LDAP - A BAGLAYIRIQ VE DAHA SONRA LDAP SERVERI RESTART EDIRIK.

$thumbprint=($mycert.Thumbprint | Out-String).Trim();

$certStoreLoc='HKLM:/Software/Microsoft/Cryptography/Services/NTDS/SystemCertificates/My/Certificates';

if (!(Test-Path $certStoreLoc)){
    New-Item $certStoreLoc -Force;
    };

Copy-Item -Path HKLM:/Software/Microsoft/SystemCertificates/My/Certificates/$thumbprint -Destination $certStoreLoc;

# DAHA SONRA LDAP SERTIFIKATINI BASE64 FORMATINDA EXPORT EDIRIK. PRIVATE KEY EHTIYAC YOXUR EXPORT ETMEYE.
# SERTIFIKATI GITLAB SERVERINE /srv/gitlab/config/trusted-certs DIREKTORIYASINA KOCURURUK. 
# FAYLI KOCURERKEN SONLUGUNU .pem TEYIN EDIRIK.
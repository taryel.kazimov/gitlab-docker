# UBUNTU UZERINDE DOCKER QURULMASI: https://docs.docker.com/engine/install/ubuntu/

# DOCKER UCUN TELEB OLUNAN EMELIYYAT SISTEMI PAKETLERINI YUKLEYIRIK.
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# RESMI DOCKER GPG ACARLARINI SISTEME ELAVE EDIRIK.

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# DOCKER UCUN STABIL REPONU HARDCODE EDIRIK.
 echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# SISTEMI UPDATE EDIRIK VE DOCKERI QURURUQ.

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install docker-ce docker-ce-cli containerd.io

# SERVISI YOXLAYIRIQ
sudo systemctl status docker

# ISIMIZI RAHATLADIRIQ.
sudo usermod -aG docker $USER
su - ${USER}
docker run hello-world
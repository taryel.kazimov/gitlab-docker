# domain.cnf FAYLINI YARADIRIQ VE DAXILINDE BIZE UYGUN DEYISIKLIKLER EDIRIK.
# ASAGIDAKI SETIRIN KOMEYI ILE SERTIFIKAT YARADIRIQ. SONDA domain.cnf FAYLINDAN MELUMATLARIN GOTURULMESI NEZERDE TUTULUR.
openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout server.key -days 3560 -out server.crt -config domain.cnf

# SERTIFIKAT VE PRIVATE KEY YARADILANDAN SONRA ONLARI DAYSIYIRIQ GITLAB/CONFIG/SSL QOVLUGUNA
# DOCKER COMPOSE FAYLINDA ASAGIDAKI ELAVELERI EDIRIK. EXTERNAL URL HTTPS OLARAQ DEYISIRIK.

external_url 'https://git.contoso.com'
nginx['enable'] = true
nginx['client_max_body_size'] = '250m'
nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "/etc/gitlab/ssl/server.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/server.key"
nginx['ssl_protocols'] = "TLSv1.2 TLSv1.3"